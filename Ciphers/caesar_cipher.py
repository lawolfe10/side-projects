import re

letter_dictionary = {1: 'A', 2: 'B', 3: 'C', 4: 'D', 5: 'E', 6: 'F', 7: 'G', 8: 'H', 9: 'I', 10: 'J', 11: 'K', 12: 'L', 13: 'M', 14: 'N',
					15: 'O', 16: 'P', 17: 'Q', 18: 'R', 19: 'S', 20: 'T', 21: 'U', 22: 'V', 23: 'W', 24: 'X', 25: 'Y', 26: 'Z', ' ': ' '}
number_dictionary = {'A': 1, 'B': 2, 'C': 3, 'D': 4, 'E': 5, 'F': 6, 'G': 7, 'H': 8, 'I': 9, 'J': 10, 'K': 11, 'L': 12, 'M': 13, 'N': 14,
					'O': 15, 'P': 16, 'Q': 17, 'R': 18, 'S': 19, 'T': 20, 'U': 21, 'V': 22, 'W': 23, 'X': 24, 'Y': 25, 'Z': 26, ' ': ' '}
#converts message to all uppercase for less coding and then uses a dictionary to convert all the letters to numbers for shifting later
def convert(convert_message):
	numeric_message = ''
	message_to_convert = re.sub('[^a-zA-Z ]', '', convert_message.upper())
	for letter in message_to_convert:
		numeric_message += str(number_dictionary[letter]) + '|'
	return numeric_message[:-1]
#takes a message and the shift for encryption/decryption
def shift(shift_message, shift_position):
	message_encryption_decryption_finished = ''
	shift_list = shift_message.split('|')
	for number in shift_list:
		if number != ' ':
			number = int(number) + shift_position
			if number > 26:
				number = number - 26
			message_encryption_decryption_finished += str(letter_dictionary[number])
		else:
			message_encryption_decryption_finished += str(letter_dictionary[number])
	return message_encryption_decryption_finished
#asks the user for a positive integer and returns the shift to be used later for encryption/decryption
def choose_shift():
	get_shift = True
	while get_shift == True:
		user_shift = re.sub('[^0-9]', '', 
					 input('Please enter the shift you would like to use on the message and then press enter (should be a positive integer): '))
		if user_shift == '':
			print('\nWARNING! Integer expected. Please enter a positive integer to continue.\n')
		elif user_shift != '':
			if int(user_shift) > 26:
				user_shift = user_shift%26
			get_shift = False
		else:
			print('\nWARNING! Integer expected. Please enter a positive integer to continue.\n')
	return int(user_shift)

if __name__ == "__main__":
    	
	print('############################################################################')
	print('############################################################################')
	print('##     ______                              _______       __               ##')
	print('##    / ____/___ ____  _________ ______   / ____(_)___  / /_  ___  _____  ##')
	print('##   / /   / __ `/ _ \/ ___/ __ `/ ___/  / /   / / __ \/ __ \/ _ \/ ___/  ##')
	print('##  / /___/ /_/ /  __(__  ) /_/ / /     / /___/ / /_/ / / / /  __/ /      ##')
	print('##  \____/\__,_/\___/____/\__,_/_/      \____/_/ .___/_/ /_/\___/_/       ##')
	print('##                                            /_/                         ##')
	print('##                                                                        ##')
	print('############################################################################')
	print('############################################################################\n\n')
	print('NOTE: This program can only handle alphabetic characters. Anything not alphabetic will be removed.')
	print('      In addition, all messages will be output to uppercase letters.\n\n')	
	while True:
    		
		user_choice = input('\nWould you like to [e]ncrypt, [d]ecrypt, or e[x]it: ')

		if user_choice.strip().lower() == 'e':
			print('\n########################################')
			print('#                                  _   #')
			print('#  ___ _ __   ___ _ __ _   _ _ __ | |_ #')
			print('# / _ \ \'_ \ / __| \'__| | | | \'_ \| __|#')
			print('#|  __/ | | | (__| |  | |_| | |_) | |_ #')
			print('# \___|_| |_|\___|_|   \__, | .__/ \__|#')
			print('#                      |___/|_|        #')
			print('########################################\n')
				
			selected_user_shift = choose_shift()
			user_message = input('\nPlease enter the message to be encrypted (non-alphabetic characters will be stripped out): ')
			print('\nEncrypted Message:   ' + shift(convert(user_message), selected_user_shift) + '\n')
			x = input('Press enter to continue. ')
				
		elif user_choice.strip().lower() == 'd':
			print('\n########################################')
			print('#     _                            _   #')
			print('#  __| | ___  ___ _ __ _   _ _ __ | |_ #')
			print('# / _` |/ _ \/ __| \'__| | | | \'_ \| __|#')
			print('#| (_| |  __/ (__| |  | |_| | |_) | |_ #')
			print('# \__,_|\___|\___|_|   \__, | .__/ \__|#')
			print('#                      |___/|_|        #')
			print('########################################\n')

			decrypt = True
			while decrypt == True:
				choice = input('\nWould you like to iterate through all shifts or do you know which shift you would like to use?\n\n   1. [I]terate through all options\n   2. [S]elect a shift for the cipher\n\nChoice: ')
				print('\n')
				if choice.strip().lower() == 'i' or choice == '1':
					user_message = input('\nPlease enter the message to be decrypted (non-alphabetic characters will be stripped out): ')
					for shiftElement in range(1,26):
						print('\nDecrypted Message:   ' + shift(convert(user_message), shiftElement) + '\n')
						next = input('Press enter to try the next shift or type in \'exit\' without the quotes to exit the loop. ')
						if next.strip().lower() == 'exit':
							break
				elif choice.strip().lower() == 's' or choice == '2':
					selected_user_shift = choose_shift()
					user_message = input('\nPlease enter the message to be decrypted (non-alphabetic characters will be stripped out): ')
					print('\nDecrypted Message:   ' + shift(convert(user_message), selected_user_shift) + '\n')
					decrypt = False	
					x = input('Press enter to continue. ')
				else:
					print('\nYou did not make a valid choice, please try again.\n')

		elif user_choice.strip().lower() == 'x':
			break

		else:
			print('\nYou did not make a valid choice, please try again.\n')