import re
while True:
    #sets the first and second fibonacci numbers
    first_fib, next_fib = 1, 1
    #asks the user which point in the series they want to find
    user_choice = re.sub('[^0-9]', '', 
					 input("What point in the fibonacci series would you like to go to: "))
    #first if just says if it is one of the first two then just print one
    #the else loops through the choices going through the series finally returning the answer at the end
    if int(user_choice) == 1 or int(user_choice) == 2:
        print('1')
    else:
        for x in range(3,int(user_choice)+1):
            user_result = first_fib + next_fib
            first_fib, next_fib = next_fib, user_result
        print(user_result)