checkNum = [3, 5]
checkName = ["fizz", "buzz"]
while True:
    start = input("What number would you like to start on (type an integer): ")
    end=input("What number would you like to end on (type an integer): ")
    for num in range(int(start), int(end)+1):
        ans=""  
        for check in range(len(checkNum)):
            if num%checkNum[check] == 0:
                ans+=checkName[check]     
        if ans == "":
            ans+=str(num)  
        print(ans)
input("\nend")